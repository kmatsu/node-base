import sinon from 'sinon';
const dynamodbUtil = require('../utility/dynamodb');

describe('dynamodb', () => {
  let dynamodb: any, DBQueryStub: any;
  dynamodb = require('./dynamodb');

  beforeEach(() => {
    dynamodb = require('./dynamodb');
    DBQueryStub = sinon
      .stub(dynamodbUtil.client, 'query')
      .returns({ promise: () => Promise.resolve({ Items: ['dummy'] }) });
  });

  afterEach(() => {
    sinon.restore();
  });

  it('ggetItemsAll returns data', async () => {
    const response = await dynamodb.getItemsAll();
    expect(DBQueryStub.called).toBe(true);
    expect(response).toEqual(['dummy']);
  });

  it('getItemsByCategoryId returns data', async () => {
    const mockCategoryId = 'test-category-id';
    const response = await dynamodb.getItemsByCategoryId(mockCategoryId);
    expect(DBQueryStub.called).toBe(true);
    expect(response).toEqual(['dummy']);
  });

});
