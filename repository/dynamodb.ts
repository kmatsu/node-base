const dynamodb = require('../utility/dynamodb');

export const getItemsAll = async () => {
  const params = {
    TableName: process.env.ITEM_TABLE,
    Limit: 100,
  };

  const list = await dynamodb.client.query(params).promise();
  return list && list.Items;
};

export const getItemsByCategoryId = async (category_id: string) => {
  const params = {
    TableName: process.env.ITEM_TABLE,
    IndexName: 'category_index',
    KeyConditionExpression: 'category_id = :value',
    ExpressionAttributeValues: {
        ':value': category_id,
      },
    Limit: 100,
  };

  const list = await dynamodb.client.query(params).promise();
  return list && list.Items;
};
