'use strict';

const dynamodbRepository = require('../repository/dynamodb');

export const getItemsAll = async () => {
  const response = await dynamodbRepository.getItemsAll();
  return response;
};

export const getItemsByCategoryId = async (category_id: string) => {
  const response = await dynamodbRepository.getItemsByCategoryId(category_id);
  return response;
};
