import sinon from 'sinon';

const dynamodbRepository = require('../repository/dynamodb');

describe('dynamodb', () => {
  let dynamodbRepositoryGetItemAllStub: any;
  let dynamodbRepositoryGetItemsByCategoryIdStub: any;
  let itemService: any;

  beforeEach(() => {
      itemService = require('./item');
      dynamodbRepositoryGetItemAllStub = sinon
        .stub(dynamodbRepository, 'getItemsAll')
        .returns([{ dummy: 1 }]);
      dynamodbRepositoryGetItemsByCategoryIdStub = sinon
        .stub(dynamodbRepository, 'getItemsByCategoryId')
        .returns([{ dummy: 1 }]);
    });
  afterEach(() => {
      sinon.restore();
    });

  it('getItemsAll returns data', async () => {
        const response = await itemService.getItemsAll();
        expect(dynamodbRepositoryGetItemAllStub.called).toBe(true);
        expect(response).toEqual([{ dummy: 1 }]);
      });

  it('getItemsByCategoryId returns data', async () => {
        const mockCategoryId = 'test-category-id';
        const response = await itemService.getItemsByCategoryId(mockCategoryId);
        expect(dynamodbRepositoryGetItemsByCategoryIdStub.called).toBe(true);
        expect(response).toEqual([{ dummy: 1 }]);
      });

});

