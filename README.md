# install

```bash
yarn install
serverless dynamodb install --stage dev
serverless dynamodb migrate --stage dev
```
# run on local
```bash
yarn run offline
```

# url on local
http://0.0.0.0:4000/dev/api/items