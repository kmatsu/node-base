describe('offline', () => {
  let dynamodb: any;
  beforeEach(() => {
    process.env.IS_OFFLINE = 'true';
    dynamodb = require('./dynamodb');
  });

  afterAll(() => {
    process.env.IS_OFFLINE = undefined;
  });

  it('should getOption return local configurations', () => {
    expect(dynamodb.client.options.region).toBe('local-env');
  });
});
