'use strict';

import { DynamoDB, Credentials } from 'aws-sdk';

const isOffline = process.env.IS_OFFLINE;

function getOption(isOffline: string|undefined) {
  if (isOffline) {
    return {
      ...(isOffline && {
          region: 'local-env',
          endpoint: 'http://localhost:8000',
          convertEmptyValues: true,
          sslEnabled: false,
          credentials: new Credentials({
              accessKeyId: '',
              secretAccessKey: '',
            }),
        }),
    };
  }
  return { convertEmptyValues: true };
}
const options: Object = getOption(isOffline);

export const client = new DynamoDB.DocumentClient(options);
export const dynamodb = new DynamoDB(options);
