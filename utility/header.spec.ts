import { getResponseHeaders } from './header';

describe('header', () => {

  it('should getResponseHeaders returns appropriate headers', () => {
    expect(getResponseHeaders()).toEqual({
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': false,
    });
  });

});
