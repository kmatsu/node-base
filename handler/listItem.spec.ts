import sinon from 'sinon';
import { getResponseHeaders } from '../utility/header';
const itemService = require('../service/item');

const mockEvent = () => ({
});

describe('listItem', () => {
  let mod: any;
  let itemServiceStub: any;
  beforeEach(() => {
    mod = require('./listItem');
    itemServiceStub = sinon
      .stub(itemService, 'getItemsAll')
      .returns(Promise.resolve([{ dummy: 1 }]));
  });
  afterEach(() => {
    sinon.restore();
  });

  it('should be successful access', async () => {
    const event = {
      ...mockEvent(),
    };
    const response = await mod.handler(event);
    expect(itemServiceStub.called).toBe(true);
    const { headers } = response;
    expect(headers).toEqual(getResponseHeaders());
    expect(response.statusCode).toBe(200);
    const body = JSON.parse(response.body);
    expect(body).toEqual([{ dummy: 1 }]);
  });

  it('should be fail and happened an error', async () => {
    itemServiceStub.restore();
    itemServiceStub = sinon
    .stub(itemService, 'getItemsAll')
    .throws(new Error('dummy error'));
    const event = {
      ...mockEvent(),
    };
    const response = await mod.handler(event);
    console.log(response);
    expect(itemServiceStub.called).toBe(true);
    const { headers } = response;
    expect(headers).toEqual(getResponseHeaders());
    expect(response.statusCode).toBe(500);
    const body = JSON.parse(response.body);
    expect(body).toBe('dummy error');
  });

});
