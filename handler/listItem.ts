'use strict';

import { getResponseHeaders } from '../utility/header';
const itemService = require('../service/item');

module.exports.handler = async (event: any) => {
  try {
    console.log(event); // TODO
    const response = await itemService.getItemsAll();
    return {
      statusCode: 200,
      headers: getResponseHeaders(),
      body: JSON.stringify(response),
    };
  } catch (error) {
    return {
      statusCode: 500,
      headers: getResponseHeaders(),
      body: JSON.stringify(error.message),
    };
  }
};
